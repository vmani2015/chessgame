/* File name : ChessPiecesManager.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 * Vinayak MANI - (S3496268)
 *  
 * SH : File Creator 
 * VM : Added getFullName(), getFullColor(), checkIsSplittable()
 * VM : Amended merge() and split()
 * 
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import Controller.MovesController;

/*
 * This class is part of the Prototype Design Pattern
 */
public class ChessPiecesManager  implements Serializable {
    
    private ArrayList<ChessPiece> chessPiecesArray;
    private String rook = "rook";
    private String bishop = "bishop";
    private String knight = "knight";
    private String starCatcher = "grower";
    private String chameleon = "chameleon";
    private String barrier = "barrier";
    private List<String> movableChess;

    
    public ChessPiecesManager(MovesController mCtrl){
        chessPiecesArray = new ArrayList<ChessPiece>();
        movableChess = new ArrayList<String>();
        movableChess.add(rook);
        movableChess.add(bishop);
        movableChess.add(knight);
//        movableChess.add(starCatcher);
//        movableChess.add(chameleon);
        initializeChessPieces(mCtrl);
    }
    
    /*
     * Create a set of chess piece prototype with basic moves
     * New pieces are cloned from these prototypes
     */
    public void initializeChessPieces(MovesController mCtrl){
        
        MoveStraight moveStraight = mCtrl.getMoveS();
        MoveDiagonal moveDiagonal = mCtrl.getMoveD();
        MoveLShape moveLShape = mCtrl.getMoveL();
        
        for (String cName : movableChess){
            
            ChessPiece chess = null;

            if(cName.equals(rook))
            {
            	chess = new ChessPiecePrototype(moveStraight);
            }     
            else if(cName.equals(bishop))
            {
            	chess = new ChessPiecePrototype(moveDiagonal);
            }
            else if(cName.equals(knight))
            {
            	chess = new ChessPiecePrototype(moveLShape);
            }
            else if (cName.equals(starCatcher))
            {
            	chess = new DistanceDecorator(new ChessPiecePrototype(moveStraight));
            }
            else if (cName.equals(chameleon))
            {
            	chess = new ColorDecorator(new ChessPiecePrototype(moveLShape));
            }
                 
            chessPiecesArray.add(chess);
            chess.setProperties(cName,null,null);
        }
        
        int customPoint = 1;
        ChessPiece barrierCP = new ChessPiecePrototype(customPoint);
        barrierCP.setProperties(barrier,null,null);
        chessPiecesArray.add(barrierCP);

    }

    
    public ChessPiece findAndClone(String name, String chessColor, String squareColor)
            throws CloneNotSupportedException
    {
        
        ChessPiece cp = null;

        for (ChessPiece chess: chessPiecesArray)

            if(chess.getName().equals(name))
            {
                cp = (ChessPiece) chess.clone();
                cp.setProperties(name, chessColor, squareColor);
            }

        
        return cp;
    }

    /*
     * Check chess's name before perform cloning
     */
    public ChessPiece createChess(String chessName, String player, String squareColor){
        
        if(chessName.length() > 3)
            chessName = chessName.substring(0, 1);
        
        ChessPiece chess = null;
        
        try {
            chess = findAndClone(getFullName(chessName), player, squareColor);
        } catch (CloneNotSupportedException e) {

            e.printStackTrace();

        }
        
        return chess;
    }

    public ChessPiece merge(ChessPiece destinationChess, ChessPiece currentChess)
    {
        
        String destinationName = destinationChess.getName();
        String currentName = currentChess.getName();
        
        //Fixed so that the substring is only taken when a single piece is involved.
        if (currentName.length() > 2)
            currentName = currentName.substring(0, 1); 

        if (destinationName.length() > 2)
            destinationName = destinationName.substring(0, 1);
        
        String originColor = destinationChess.getColor();
        String originSquareColor = destinationChess.getSquareColor();
                
        String[] comboArray = {currentName, destinationName};
        Arrays.sort(comboArray);
        String combo = comboArray[0] + comboArray[1];
        
        //Instantiate new merged pieces
        ArrayList<Move> moves = new ArrayList<Move>();
        moves.addAll(destinationChess.getMoves());
        moves.addAll(currentChess.getMoves());
        
        ChessPiece chess = new ChessPiecePrototype(moves);
        chessPiecesArray.add(chess);
        
      //Set properties for new piece
        if(combo.length() > 2)
            chess.setProperties("bkr", originColor, originSquareColor);
        else
            chess.setProperties(combo, originColor, originSquareColor);
        
        
        return chess;
    }
    
    public String getFullName(String input)
    {
        String name = "";
        
        if ( input.equals("r"))
        {
            name = rook;
        }
        else if ( input.equals("b"))
        {
            name = bishop;
        }
        else if ( input.equals("k"))
        {
            name = knight;
        }
        
        return name;
    }
    
    public String getFullColor(String input)
    {
        String name = "";
        
        if ( input.equals("w"))
        {
            name = "white";
        }
        else if ( input.equals("b"))
        {
            name = "black";
        }
        
        return name;
    }
    
    public String[] checkIsSplittable(String name){
        
        String[] splitMsgs = null;
            
        if (name.equals("br")){
            splitMsgs = new String[]{"Bishop", "Rook"};
        }
        else if (name.equals("kr")){
            splitMsgs = new String[]{"Knight", "Rook"};
        }
        else if (name.equals("bk")){
            splitMsgs = new String[]{"Bishop", "Knight"};
        }
        else if (name.equals("bkr")){
            splitMsgs = new String[]{"Bishop", "Knight", "Rook", "Bishop + Rook", "Bishop + Knight", "Knight + Rook"};
        }
        
        return splitMsgs;
    }
    
    public ChessPiece[] split(ChessPiece currentSelectChess, String newChess)
    {
        
        String name = currentSelectChess.getName();
        String squareColor = "";
        String player = "";

        int max = 0;

        if(name.contains("b") && name.contains("k") && name.contains("r")) 
          max = 3;

        else
            max = 2;
        
        String[] chessNames = new String[max];
        ChessPiece[] newChessPiece = new ChessPiece[max];

        
        for (int i = 0; i < max; i++){

            chessNames[i] = name.substring(i, i + 1);
            
            //To get the full name of the chess board
            if ( chessNames[i].length() == 1)
            {
                chessNames[i] = getFullName(chessNames[i]);
            }
            
            squareColor = currentSelectChess.getSquareColor();
            player = currentSelectChess.getColor();

            newChessPiece[i] = createChess(chessNames[i], player, squareColor);
        }
        
        // 3 piece combo
        if (max == 3)
        {
            
            if ( newChess.length() > 3)
            {
            
                String mergeChess = "";
            
                ChessPiece to_move = null;
            
                if(chessNames[0] == newChess)
                {
                    chessNames[1] = chessNames[1].substring(0, 1);
                    chessNames[2] = chessNames[2].substring(0, 1);
                    mergeChess = chessNames[1]+chessNames[2];
                    to_move = newChessPiece[0];
                
                }
                else if (chessNames[1] == newChess)
                {
                 chessNames[0] = chessNames[0].substring(0, 1);
                 chessNames[2] = chessNames[2].substring(0, 1);
                 mergeChess = chessNames[0]+ chessNames[2];
                 to_move = newChessPiece[1];
                
                }
                else if (chessNames[2] == newChess)
                {
                chessNames[0] = chessNames[0].substring(0, 1);
                chessNames[1] = chessNames[1].substring(0, 1);
                mergeChess = chessNames[0]+ chessNames[1];
                to_move = newChessPiece[2];
                }
            
                //These are the two pieces that we have to merge.
                ChessPiece p1 = createChess(mergeChess.substring(0, 1), player, squareColor);
                ChessPiece p2 = createChess(mergeChess.substring(1, 2), player, squareColor);
            
            
                //This code merges the chess pieces
                ChessPiece new_merged = merge(p1, p2);
            
                newChessPiece[0] = to_move;
                newChessPiece[1] = new_merged;
                newChessPiece[2] = null;
            }
            else
            {
                //These are the two pieces that we have to merge.
                ChessPiece p1 = createChess(newChess.substring(0, 1), player, squareColor);
                ChessPiece p2 = createChess(newChess.substring(1, 2), player, squareColor);
            
                //This code merges the chess pieces
                ChessPiece new_merged = merge(p1, p2);
            
                //Use a regex expression to get the piece left
                
                String piece_left_name = "";
                
                piece_left_name = name.replaceAll("["+new_merged.getName()+"]","");
                
                piece_left_name = getFullName(piece_left_name);
                
                ChessPiece cp_left = createChess(piece_left_name, player, squareColor);
                
                newChessPiece[0] = new_merged;
                newChessPiece[1] = cp_left;
                newChessPiece[2] = null;
            }
                
        }

        
        return newChessPiece;
            
    }
    
    public List<String> getChessNames() {
        return movableChess;
    }


}
