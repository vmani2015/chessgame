/* File Name : MoveStraight.java
 * 
 * Author(s):
 * Vinayak MANI (S3496268)
 *  
 * File created by Vinu
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

import Controller.MovesController;


public class MoveStraight implements Move,Serializable{
    
    private MovesController mCtrl;
	
	public MoveStraight(MovesController ctrl){
        this.mCtrl = ctrl;
    }
	
	public ArrayList<String> suggestMoves(int startx,int starty, int[] limit)
	{
		
		ArrayList<String> temp = new ArrayList<String>();
		
		int heightLimit = limit[1] -1;
		int widthLimit = limit[0] -1;
	
	
		//Work out up sqaures
		int tempx = startx;
		int tempy = starty;
		
		while (tempy > 0)
		{
			tempy = tempy -1;
			if (mCtrl.isEmptySquare(tempx, tempy))
			{
				temp.add(tempx+","+tempy);
			}
			else
			{
				temp.add(tempx+","+tempy);
				break;
			}

		}
		
		//Work out down sqaures
				tempx = startx;
				tempy = starty;
				
				while (tempy < heightLimit)
				{
					tempy = tempy +1;
					
					if (mCtrl.isEmptySquare(tempx, tempy))
					{
						temp.add(tempx+","+tempy);
					}
					else
					{
						temp.add(tempx+","+tempy);
						break;
					}
					
					
				}
				
				//Work out right sqaures
				tempx = startx;
				tempy = starty;
				
				while (tempx < widthLimit)
				{
					tempx = tempx +1;
					
					if (mCtrl.isEmptySquare(tempx, tempy))
					{
						temp.add(tempx+","+tempy);
					}
					else
					{
						temp.add(tempx+","+tempy);
						break;
					}
				}
				
				//Work out left sqaures
				tempx = startx;
				tempy = starty;
				
				while (tempx > 0)
				{
					tempx = tempx -1;

					if (mCtrl.isEmptySquare(tempx, tempy))
					{
						temp.add(tempx+","+tempy);
					}
					else
					{
						temp.add(tempx+","+tempy);
						break;
					}
				}	
		return temp;
	}

}
