/* File Name : MoveLShape.java
 * 
 * Author(s):
 * Vinayak MANI (S3496268)
 *  
 * File created by Vinu
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

import Controller.MovesController;

public class MoveLShape implements Move,Serializable{
	
	private MovesController mCtrl;
    
    public MoveLShape(MovesController ctrl){
        this.mCtrl = ctrl;
    }
	
	public ArrayList<String> suggestMoves(int startx, int starty, int[] limit)
	{

		ArrayList<String> temp = new ArrayList<String>();
		int heightLimit = limit[1] -1;
        int widthLimit = limit[0] -1;
	
		//Work out up-left squares
		int tempx = startx;
		int tempy = starty;
		
		tempy = tempy -2 ;
		tempx = tempx - 1;
		
		if (tempy >= 0 && tempx >= 0)	
		{
			temp.add(tempx+","+tempy);
			
		}	
		
		//Work out up-right squares
				tempx = startx;
				tempy = starty;
				
				/*while (tempx < 5 && tempy >=2)
				{*/
				tempy = tempy -2 ;
				tempx = tempx + 1;
				
				if (tempy >= 0 && tempx <= widthLimit)	
				{
					
					temp.add(tempx+","+tempy);
				}
		
				
		//Work out down left sqaures
				tempx = startx;
				tempy = starty;
				
				tempy = tempy +2 ;
				tempx = tempx - 1;
				if (tempy <= heightLimit  && tempx >= 0)
				{
					temp.add(tempx+","+tempy);
				}
		
				
		//Work out down right sqaures
				tempx = startx;
				tempy = starty;
				
				tempy = tempy +2 ;
				tempx = tempx + 1;
				
				if (tempy <= heightLimit  && tempx <= widthLimit)
				{
					temp.add(tempx+","+tempy);
				}	
				
		//Work out left-up sqaures
				tempx = startx;
				tempy = starty;
				
				tempx = tempx - 2;
				tempy = tempy - 1 ;
				
				
				if (tempy >=0  && tempx >=0)
				{
					temp.add(tempx+","+tempy);
				}
				
		//Work out left-down squares
				tempx = startx;
				tempy = starty;
				
				tempx = tempx - 2;
				tempy = tempy + 1 ;
				
				
				if (tempy <= heightLimit  && tempx >=0)
				{
					temp.add(tempx+","+tempy);
				}
				
		//Work out right-up sqaures
				tempx = startx;
				tempy = starty;
				
				tempx = tempx + 2;
				tempy = tempy - 1 ;
				
				
				if (tempy >=0  && tempx <= widthLimit)
				{
					temp.add(tempx+","+tempy);
				}
				
		//Work out right-down squares
				tempx = startx;
				tempy = starty;
				
				tempx = tempx + 2;
				tempy = tempy + 1 ;
				
				
				if (tempy <= heightLimit  && tempx <= widthLimit)
				{
					temp.add(tempx+","+tempy);
				}
				
		return temp;
	}

	
}
