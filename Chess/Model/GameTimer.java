/* File Name : GameTimer.java
 * 
 * Author(s):
 * Vinayak MANI (S3496268)
 *  
 * File created by Vinu
 */


package Model;


import java.io.Serializable;

import javax.swing.Timer;

import Controller.GUIController;


public class GameTimer implements Serializable {
    
  //Needed to access the timer
    private static Timer timertouse;
    private int turn_time;
    private static GUIController guiCtrl;
    
    //Needed for load game
    private int loaded_turn_time = 0;
    
    public GameTimer(GUIController gCtrl){
        this.guiCtrl = gCtrl;
        if (loaded_turn_time != 0)
        {
        	turn_time = loaded_turn_time;
        }
        else
        {
        	turn_time = 25;  //This is the default time. 
        	//We could put a GUI here accept user input.
        }
        timertouse = new Timer(1000, guiCtrl.new TimerListener(turn_time));
    }
     
    public int getTurnTime()
    {
    	//Used to load the turn time from the save file
        if ( loaded_turn_time != 0)
        {
        	return getLoadedTurnTime();
        }
        else
        {	
        	return turn_time;
        }	
    }
    
    public void setTurnTime(int pturn_time)
    {
    	this.turn_time = pturn_time;
    }
    
    //Needed for load 
    public void setLoadedTurnTime(int ptime)
    {
    	loaded_turn_time = ptime;
    }
    
    public int getLoadedTurnTime()
    {
    	return this.loaded_turn_time;
    }

    //Needed for timer
    public static void resetTimer(int param)
    {
      timertouse = null;
      timertouse = new Timer(1000, guiCtrl.new TimerListener(param));
    }
    
    public static void stopTimer()
    {
        timertouse.stop();
    }
    
    public static void startTimer(){
        timertouse.start();
    }


    public String getDurationString(int pseconds) 
    {
        int hours;
        int minutes;
        int seconds;
        
        String hourspart;
        String minutespart;
        String secondspart;


        hours = pseconds / 3600;

        minutes = (pseconds % 3600) / 60;

        seconds = pseconds % 60;


        if ( hours < 10)
        {
            hourspart = "0"+Integer.toString(hours);
        }

        else
        {
            hourspart = Integer.toString(hours);
        }

       
        if ( minutes < 10)
        {
            minutespart = "0"+Integer.toString(minutes);
        }
        else
        {
            minutespart = Integer.toString(minutes);
        }

       

        if ( seconds < 10)
        {
            secondspart = "0"+Integer.toString(seconds);
        }
        else
        {
            secondspart = Integer.toString(seconds);
        }

       
        return (hourspart) + " : " + (minutespart) + " : " + (secondspart);

    }



}