/* File name : Player.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * SH : File creator
 */

package Model;

import java.io.Serializable;

public class Player implements Serializable {
    
    private String name;
    private int point;
    
    public Player(String name){
        this.name = name;
        this.point = 0;
    }
    
    public void addPoint(int givenPoint){
        point += givenPoint;
    }

    public int getPoint(){
        return point;
    }
    
    public String getName(){
        return name;
    }
    
    //Needed for Load game
    public void setPoint(int ppoint)
    {
    	point = ppoint;
    }

}
