/* File Name : MoveDiagonal.java
 * 
 * Author(s):
 * Vinayak MANI (S3496268)
 *  
 * File created by Vinu
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import Controller.MovesController;


public class MoveDiagonal implements Move,Serializable{

    private MovesController mCtrl;
    
    public MoveDiagonal(MovesController ctrl){
        this.mCtrl = ctrl;
    }
	
	public ArrayList<String> suggestMoves(int startx, int starty, int[] limit)
	{
		
		ArrayList<String> temp = new ArrayList<String>();
		int heightLimit = limit[1] -1;
        int widthLimit = limit[0] -1;
	
	
		//Work out up left up sqaures
		int tempx = startx;
		int tempy = starty;
		
		while (tempy > 0 && tempx > 0)
		{
			tempy = tempy -1;
			tempx = tempx -1;
			
			if (mCtrl.isEmptySquare(tempx, tempy))
			{
				temp.add(tempx+","+tempy);
			}
			else
			{
				temp.add(tempx+","+tempy);
				break;
			}
			
		}
		
		//Work out left down sqaures
				tempx = startx;
				tempy = starty;
				
				while (tempx > 0 && tempy < heightLimit)
				{
					tempx = tempx - 1;
					tempy = tempy +1;
					
					if (mCtrl.isEmptySquare(tempx, tempy))
					{
						temp.add(tempx+","+tempy);
					}
					else
					{
						temp.add(tempx+","+tempy);
						break;
					}
					
				}
				
				//Work out right up sqaures
				tempx = startx;
				tempy = starty;
				
				while (tempy > 0  && tempx < widthLimit)
				{
					tempx = tempx +1;
					tempy = tempy- 1;
					
					if (mCtrl.isEmptySquare(tempx, tempy))
					{
						temp.add(tempx+","+tempy);
					}
					else
					{
						temp.add(tempx+","+tempy);
						break;
					}
				}
				
				//Work out right down sqaures
				tempx = startx;
				tempy = starty;
				
				while (tempx < widthLimit && tempy < heightLimit)
				{
					tempx = tempx +1;
					tempy = tempy + 1;

					if (mCtrl.isEmptySquare(tempx, tempy))
					{
						temp.add(tempx+","+tempy);
					}
					else
					{
						temp.add(tempx+","+tempy);
						break;
					}
				}	
		return temp;
	}


}
