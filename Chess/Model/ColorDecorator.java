/* File name : ColorDecorator.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * SH : File Creator 
 * 
 */
package Model;

import java.util.ArrayList;

/*
 * This class is part of the Decorator Design Pattern
 * When this class is instantiated, it will pass the ChessPiece
 * that is being decorated to it's superclass
 */
public class ColorDecorator extends ChessDecorator{
	
	private String name;
    private String playerColor;
    private String squareColor;
    private int point = 5;
    private ArrayList<Move> moves;
    private String decoName;
	
	public ColorDecorator(ChessPiece base){
	    super(base);

	}


    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }


    public void setProperties(String name, String color, String squareColor)
    {
        this.name = name;
        this.playerColor = color;
        this.squareColor = squareColor;
    }


    public String getColor() {
        return this.playerColor;
    }


    public String getName() {
        if(this.decoName != null)
            return this.decoName;
        else
            return this.name;
    }


    public String getSquareColor() {
        return this.squareColor;
    }


    public int getPoint() {
        return this.point;
    }


    public ArrayList<Move> getMoves() {
        return moves;
    }

    /*
     * Get the suggested moves from decorated base object
     * @see Model.ChessDecorator#getSuggestMoves(int, int, int[])
     */
    public ArrayList<String> getSuggestMoves(int startx, int starty, int[] limits) 
    {   
        return decorated.getSuggestMoves(startx, starty, limits);
    }
    
    public void setDecoColor(String deco){
        this.decoName = name + deco;
    }


}
