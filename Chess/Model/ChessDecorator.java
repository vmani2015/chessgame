/* File Name : ChessDecorator.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 */

package Model;

import java.io.Serializable;
import java.util.ArrayList;

/*
 * This class is part of the Decorator Design Pattern
 * This subclass of this class will pass an instance of the 
 * decorated ChessPiece and stores in the decorated variable
 */
public abstract class ChessDecorator implements ChessPiece, Serializable {

    protected ChessPiece decorated;

    
    public ChessDecorator(ChessPiece base){
        this.decorated = base;
    }

    @Override
    public abstract ArrayList<String> getSuggestMoves(int startx, int starty, int[] limits);

    @Override
    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }
    
    @Override
    public abstract void setProperties(String name, String color, String squareColor);

    @Override
    public abstract String getColor();

    @Override
    public abstract String getName();

    @Override
    public abstract String getSquareColor();
    
    @Override
    public abstract int getPoint();

    @Override
    public abstract ArrayList<Move> getMoves();
    
    
}
