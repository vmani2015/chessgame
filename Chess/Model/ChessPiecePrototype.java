/* File Name : ChessPiecePrototype.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 */

package Model;

import java.io.Serializable;
import java.util.ArrayList;


public class ChessPiecePrototype implements ChessPiece, Serializable {
 

    private String name;
    private String playerColor;
    private String squareColor;
	private int point = 5;
	private ArrayList<Move> moves = new ArrayList<Move>();
	

	public ChessPiecePrototype(ArrayList<Move> moveTypes)
	{
	    for(Move type : moveTypes)
			moves.add(type);

	    point *= moveTypes.size();
	}
	
	/* 
	 * Second constructor overloading default constructor 
	 */
	public ChessPiecePrototype(final Move move){
	    this(new ArrayList<Move>(){{add(move);}});
	}
	
	/* 
     * Third constructor for static piece (eg: barrier)
     */
    public ChessPiecePrototype(int point){
        this.moves = null;
        this.point = point;
    }

	
	public void setProperties(String name, String color, String squareColor)
	{
	    this.name = name;
	    this.playerColor = color;
	    this.squareColor = squareColor;	    
	}
	
	public String getColor()
	{
	    return this.playerColor;
	}
	
	public String getName()
	{
	    return this.name;
	}
	
	public String getSquareColor()
	{
	    return this.squareColor;
	}
	
	public int getPoint(){
	    return this.point;
	}
	
	public ArrayList<String> getSuggestMoves(int startx, int starty, int[] limits) 
	{
	    
	    ArrayList<String> suggestedMoves = new ArrayList<String>();
	    ArrayList<String> tempMoves = new ArrayList<String>();
	    
	    for(Move move : moves){
	        tempMoves.addAll(move.suggestMoves(startx, starty, limits));
	    }
	    
	    if (suggestedMoves.size() == 0){
	        suggestedMoves = tempMoves;
	    }
	    else
	    {
	        for (int i = 0; i < suggestedMoves.size(); i++){
	            for (int j = 0; j < tempMoves.size(); j++){
	                String temp = tempMoves.get(j);
	                String existingSquare = suggestedMoves.get(i);

	                if(!temp.equals(existingSquare))
	                    suggestedMoves.add(temp);
	            }
	        }
	    }
        return suggestedMoves;
    }
	
	public ArrayList<Move> getMoves(){
	    return moves;
	}

	@Override
    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }

}

