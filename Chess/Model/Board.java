/* File Name : Board.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * File created by Cynthia
 */
package Model;

import java.io.Serializable;


public class Board implements Serializable {
	
	private String[][] gameBoard;
	public int rowSize;
	public int colSize;
	private final String ROWNAME = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	
	public Board(int defaultBoardSize){
	    rowSize = defaultBoardSize;
	    colSize = defaultBoardSize;
	}
	
	public void createBoard(){
		this.gameBoard = new String[rowSize][colSize];
		setSquareName();
	}
	
	public int[] getBoardSize(){
		int[] size = {rowSize, colSize};
		return size;
	}
	
	
	public void setSquareName(){
		// set name eg: "A1"
		for (int row = 0; row < rowSize; row++) {
			for (int col = 0; col < colSize; col++) {
				gameBoard[row][col] = ROWNAME.substring(row, row + 1) + (col + 1);
			}
        }
	}
	
	public String getSquareName(int row, int col){
	    
		return gameBoard[row][col];
	}
	
	
	public int[] getSquareNo(String square){
		
		int[] squareNo = new int[2];

		for (int i = 0 ; i < rowSize; i++)
			for(int j = 0 ; j < colSize ; j++)
			{
				if ( gameBoard[i][j].equalsIgnoreCase(square))
				{
					squareNo[0] = i;
					squareNo[1] = j;
					break;
				}
			}
		
		return squareNo;
	}
	
	public void setBoardSize(int[] sizes){
	    this.rowSize = sizes[1];
	    this.colSize = sizes[0];
	    createBoard();
	}
	
}
