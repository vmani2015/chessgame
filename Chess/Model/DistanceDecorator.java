/* File name : DistanceDecorator.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * SH : File Creator 
 * 
 */
package Model;

import java.util.ArrayList;

/*
 * This class is part of the Decorator Design Pattern
 * When this class is instantiated, it will pass the ChessPiece
 * that is being decorated to it's superclass
 */
public class DistanceDecorator extends ChessDecorator{
	
	private String name;
    private String playerColor;
    private String squareColor;
    private int point = 5;
    private ArrayList<Move> moves;
    private int distance = 1;
	
	public DistanceDecorator(ChessPiece base){
	    super(base);

	}
	
	public void addDistance(){
	    this.distance += distance;
	}


    public Object clone() throws CloneNotSupportedException{
        return super.clone();
    }


    public void setProperties(String name, String color, String squareColor)
    {
        this.name = name;
        this.playerColor = color;
        this.squareColor = squareColor;
    }


    public String getColor() {
        return this.playerColor;
    }


    public String getName() {
        return this.name;
    }


    public String getSquareColor() {
        return this.squareColor;
    }


    public int getPoint() {
        return this.point;
    }


    public ArrayList<Move> getMoves() {
        return moves;
    }

    /*
     * @see Model.ChessDecorator#getSuggestMoves(int, int, int[])
     * After getting the suggested move from base object,
     * filter moves that is further than the current distance 
     */
    public ArrayList<String> getSuggestMoves(int startx, int starty, int[] limits) 
    {   

        ArrayList<String> suggestedMoves = new ArrayList<String>();
        ArrayList<String> temp = new ArrayList<String>();
        
        temp = decorated.getSuggestMoves(startx, starty, limits) ;
        
        for(String pos : temp){
           
            String[] point = pos.split(",");

            if (Integer.valueOf(point[0]) <= startx + distance && 
                Integer.valueOf(point[0]) >= startx - distance && 
                Integer.valueOf(point[1]) <= starty + distance && 
                Integer.valueOf(point[1]) >= starty - distance)
            {
                    
                suggestedMoves.add(pos);
            }
        }
        
        return suggestedMoves;
    }


}
