/* File Name : GameEngine.java
 * 
 * Author(s):
 * Vinayak MANI - (S3496268)
 *  
 * File created by Vinu
 */

package Model;

import java.io.Serializable;
import java.util.ArrayList;

import Controller.GameController;


public class GameEngine implements Serializable {
	
	private GameController gameCtrl;
	private ArrayList<String> suggestedMoves;
	private String errorMsg;
	

	public GameEngine(GameController gameCtrl){
	    this.gameCtrl = gameCtrl;
		this.errorMsg = null;
		this.suggestedMoves = null;
	}

	
	//check if there's anything pre-selected
	public boolean validFirstClick(String destination, ChessPiece currentSelectedChess){

		//User clicking on empty square on the first time
		if (currentSelectedChess == null)
		{
		    errorMsg = "Invalid move. Empty square selected.";
			return false;
		}

		//Make sure that the user has not clicked a barrier
		else if (currentSelectedChess.getName().equalsIgnoreCase("Barrier"))
		{
		    errorMsg = "Barriers cannot move. Please select a valid move";
			return false;
		}
		else
			return true;

	}
	
	
	
	public boolean validSecondClick(String origin, String destination, ChessPiece currentSelectedChess)
	{
		
		Boolean valid = false;
		GameState game = gameCtrl.getGame();
		
		//If the player did not clicks the same square twice
		if (origin.equals(destination))
		{
		    errorMsg = "Invalid move.";
		}
		else {
		    
		    //check if player clicked on the suggested square   
            for (String selected : suggestedMoves)
            {
                if (selected.equals(destination))
                {
                    valid = true;
                }    
            }

		}

		// Player selected one of the suggested squares
		if (valid)
		{

		    ChessPiece destinationChess = game.getChessPiece(destination);
		    
		    // Destination square is not empty
		    if (destinationChess != null)
		    {
		        
		        String destinationChessColor = destinationChess.getColor();

		        //check if same side (color), if yes then merge, else capture
		        if (currentSelectedChess.getColor().equalsIgnoreCase(destinationChessColor))
		        {
		            String currentChessName = currentSelectedChess.getName();
		            
		            if (validMerge(currentChessName, destinationChess.getName()))
		            {					
		                currentSelectedChess = gameCtrl.getCPManager().merge(destinationChess, currentSelectedChess);
		                gameCtrl.setCurrentChess(currentSelectedChess);
		            }

		            else
		            {
		                errorMsg = "Invalid merge.";
		                valid = false;
		            }
		        }
		        else
		        {
		            //System.out.println(currentSelectedChess.getColor()+","+destinationChessColor);
		            //System.exit(0);
		        	capture(destinationChess);
		        	
		        	// if currentSelectedChess is a decorator and destination chess is a barrier, change decorator behavior  
		            if (currentSelectedChess instanceof DistanceDecorator && destinationChess.getName() == "barrier"){
		                ((DistanceDecorator)currentSelectedChess).addDistance();
		            }
		            
		            if (currentSelectedChess instanceof ColorDecorator){
		                String decoColor = destinationChess.getColor();

		                if (decoColor.equals(""))
		                    decoColor = "blue";
		                
                        ((ColorDecorator)currentSelectedChess).setDecoColor(decoColor);
		                
                    }

		        }

		}
		else
		{ 
			errorMsg = "Invalid move.";
		}
		
		
	}
		return valid;	
	
}

	
	public boolean validMerge(String origin,String destination)
	{
		
		char[] dest = null;
		boolean canMerge = true;
		String current_char = "";
		
		//Strip the origin and the destination if they are more than 3 letters long
		if ( origin.length() > 3)
		{
			origin = origin.substring(0, 1);
		}
		
		if (destination.length() > 3)
		{
			destination = destination.substring(0, 1);
		}
		
		//Compare the origin against each letter of the destination
		//This removes the merge suggestion of br merging with kr
		//This was a flaw in the previous code.
		//After processing the destination, we then take the char array
		dest = destination.toCharArray();
		
		for (char c:dest)
		{
			current_char = Character.toString(c);
			if (origin.contains(current_char))
			{
			    canMerge = false;
			}
		}
		
		//DistanceDecorator and ColorDecorator can not merge
		if(origin.equals("g") || origin.equals("c") || destination.equals("g") || destination.equals("c"))
		    canMerge = false;
		
		//If no matches then return true
		return canMerge;
	}
	
	
	public boolean validMergeOld(String origin, String destination)
	{
		// If one of the pieces is three piece combo(bkr), no merge permitted
		
		if (origin.equalsIgnoreCase("bkr") || destination.equalsIgnoreCase("bkr"))
		{
			return false;
		}

		//If the two pieces are exactly the same, no merge permitted

		else if (origin.equalsIgnoreCase(destination))  
		{
			return false;
		}

		// If one of the pieces is a bishop and the other contains a Bishop

		else if (origin.equalsIgnoreCase("Bishop") && destination.contains("b"))  
		{
			return false;
		}

		else if (destination.equalsIgnoreCase("Bishop") && origin.contains("b"))
		{
			return false;
		}

		// If one of the pieces is a Knight and the other contains a knight
		//Checking the length so that Knight and Rook which contains "k" is considered	
				// a valid move 
		else if (origin.equalsIgnoreCase("Knight") && destination.contains("k") && destination.length() != 4)  
		{
			return false;
		}

		else if (destination.equalsIgnoreCase("Knight") && origin.contains("k") && origin.length() != 4)
		{
			return false;
		}

		// If one of the pieces is a Knight and the other contains a knight

		else if (origin.equalsIgnoreCase("Rook") && destination.contains("r"))  
		{
			return false;
		}

		else if (destination.equalsIgnoreCase("Rook") && origin.contains("r"))
		{
			return false;
		}

		else
			return true;

	}


	
	public ArrayList<String> RemoveInvalidMergeSuggestions(ChessPiece selectedChess, ArrayList<String> suggestedMoves) 
    {
        ArrayList<String> new_array = new  ArrayList<String>();
        String current_piece_name ="";
        String current_piece_colour = "";
        String suggestion_piece_name ="";
        String suggestion_piece_colour ="";
        GameState game = gameCtrl.getGame();
        
        current_piece_colour = selectedChess.getColor();
        // current_piece_name should be "r" if piece is "Rook"
        if (selectedChess.getName().length() > 3)
            {
                current_piece_name = selectedChess.getName().substring(0, 1);
            }
        else
            {
                current_piece_name = selectedChess.getName();
            }
        
        for (String move:suggestedMoves)
        {
            
        	//When the destination square is not empty
            if (game.getMap().get(move) !=null )
            {
            	
            		suggestion_piece_name = game.getMap().get(move).getName();
            		suggestion_piece_colour = game.getMap().get(move).getColor();
            		
            		// suggested_piece_name should be "r" if piece is "Rook"
            		//Only if the name is not a barrier.
            		if (suggestion_piece_name.length() > 3 && suggestion_piece_name.equals("barrier") == false)
            		{
            			suggestion_piece_name = suggestion_piece_name.substring(0, 1);
            		} 
            		
            		//If the pieces are different colors, this is a capture suggestion so add it to the array
            		//This code also works for barriers as their color is set to ""
            		if (suggestion_piece_colour.equalsIgnoreCase(current_piece_colour) == false)
            		{
            		        new_array.add(move);
            		}
            		//This is a merge suggestion
            		else
            		{
            			if (validMerge(current_piece_name, suggestion_piece_name))
            			{
            			        new_array.add(move);
            			}
            			
            		}
            		
            		
            		// ChessPiece with ChessDecorator cannot merge with own team
            		
            		if(current_piece_name.equals("g") && suggestion_piece_name.equals("barrier") == false)
                        if(suggestion_piece_colour.equals(current_piece_colour))
                            new_array.remove(move);
                        
                    else if(current_piece_name.equals("c") && suggestion_piece_name.equals("barrier") == false)
                        if(suggestion_piece_colour.equals(current_piece_colour))
                            new_array.remove(move);
                        
                    

            }
            //Otherwise immediately add it to the array
            else
            {
                    new_array.add(move);
            }
        }
        
        return new_array;
    }
	
	public ArrayList<String> suggestMove(String destination, ChessPiece currentSelectedChess){
	    
	    Board board = gameCtrl.getGameBoard();
	    
		//get gameBoard X and Y
		int[] num = board.getSquareNo(destination);
		
		
		suggestedMoves = currentSelectedChess.getSuggestMoves(num[0], num[1], board.getBoardSize());
		
		ArrayList<String> squareNames = new ArrayList<String>();

		for(String m : suggestedMoves){
			int row = Integer.parseInt(m.substring(0,1));
			int col = Integer.parseInt(m.substring(2,3));
			squareNames.add(board.getSquareName(row, col));
		}
		
		suggestedMoves = squareNames;
		
		return squareNames;
	}
	
	
	
	public String getErrorMsg(){
	    return errorMsg;
	}
	
	// method to update point in GUI after opponent being up captured
    public void capture(ChessPiece destinationChess){
        int point = destinationChess.getPoint();
        
        Player currentPlayer = gameCtrl.getPlayerController().getCurrentPlayer();     
        currentPlayer.addPoint(point);
        
        String playerName = currentPlayer.getName();
        int newPoint = currentPlayer.getPoint();
        
        gameCtrl.getGUICtrl().updatePoint(playerName,newPoint);


    }

	
	public String isGameOver(ChessPiece param) {
        
        String color_to_check = "";
        String winner_color="";
        
        //Get the color of the chess piece
        if ( param.getColor().equalsIgnoreCase("w"))
        {
            color_to_check = "b"; 
        }
        else
        {
            color_to_check = "w"; 
        }
        
        GameState game = gameCtrl.getGame();
        
        for (ChessPiece value : game.getMap().values()) 
        {
            if (value.getColor().equalsIgnoreCase(color_to_check))
            {
                return "N";
            }
        }
        
        if ( color_to_check.equalsIgnoreCase("w"))
        {
            winner_color = "b"; 
        }
        else
        {
            winner_color = "w"; 
        }

        
        return winner_color;
    }
	
	
	


}