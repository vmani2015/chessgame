/* File Name : GameState.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 * Vinayak MANI - (S3496268)
 *  
 * SH : File creator
 * VM : Added merge(), movePiece(), edited getSquare(ChessPiece chess)
 * VM : Added SaveGame() method, loadGame(), UpdateColorsInHashMap()
 */

package Model;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.swing.JFileChooser;

import Controller.GUIController;
import Controller.GameController;
import Controller.PlayerController;

public class GameState implements Serializable {
	
	private HashMap<String, ChessPiece> gameStateMap = new HashMap<String, ChessPiece>();
	private GameController gameCtrl;
    private ChessPiecesManager cpMgr;
    
    public GameState(ChessPiecesManager cpMgr, GameController ctrl){
        this.cpMgr = cpMgr;
        this.gameCtrl = ctrl;
    }
	
	
	public HashMap<String, ChessPiece> newGame(int totalPieces, Board board, String black, String white){
		
	    int rowSize = board.getBoardSize()[0];
	    int colSize = board.getBoardSize()[1];
	    
	    String player1 = white.substring(0, 1);
	    String player2 = black.substring(0, 1);	    
	    
	    List<String> names = cpMgr.getChessNames();
	    
	    int variance = 1;
	    while(names.size() < totalPieces){

	        if(variance > names.size())
                variance %= 3;

            int n = names.size() - variance;
	        
	        names.add(names.get(n));
	        
	        variance += 2;
	    }
	    
	    String[] chessNames  = names.toArray(new String[names.size()]);

	    //set white chess pieces
        int col = 0;
        int num = 0;
        String squareColor;
               
        while (col < colSize && col < totalPieces){
            
            int row = 0;
            
            squareColor = (col % 2 == 0) ? black : white;
            String squareName = board.getSquareName(row,col);
            addPieceInMap(chessNames[num], player1, squareColor, squareName);

            col++;
            num = (num < chessNames.length) ? num + 1 : 0;

        }
      
      //set black chess pieces
        col = 0;
        num = 0;
        while (col < colSize && col < totalPieces){
            
            int row = board.getBoardSize()[0] - 1;

            squareColor = (col % 2 == 0) ? white : black;
            String squareName = board.getSquareName(row,col);
            addPieceInMap(chessNames[num], player2, squareColor, squareName);

            col++;
            
            if (num < chessNames.length) 
                num++ ;
            else 
                num = 0;
        }       

		//set barrier
		 int barrierStart = rowSize / 2; 
		 
		 if(rowSize % 2 == 0)
		     barrierStart--;

	        for (int ii = barrierStart ; ii < barrierStart+2; ii++) {
	            for (int jj = 0; jj < colSize; jj++) {

	                String squareName = board.getSquareName(ii,jj);
	                addPieceInMap("barrier","","", squareName);
                    
	            }
	        }
	     
		return gameStateMap;
	}
	

	public void addPieceInMap(String name, String color, String squareColor, String squareName)
	{
	    ChessPiece newPiece = null;
	    try {
	        newPiece =  cpMgr.findAndClone(name,color,squareColor);

        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
	    
	    gameStateMap.put(squareName, newPiece);
	}
	
	public void endGame()
    {
        gameStateMap.clear();
    }
	
	public ChessPiece getChessPiece(String square)
	{
		return gameStateMap.get(square);
	}
	
	public int getPoint(String destination)
	{
	    return gameStateMap.get(destination).getPoint();
	}
	
	public void movePiece(String origin,String destination, ChessPiece currentSelectedChess)
	{		
		
		if ( origin == null)
		{
		    gameStateMap.put(destination, currentSelectedChess);
		}
		else
		{
			//Remove the old key and corresponding old chess piece
		    gameStateMap.remove(origin);
	
			//Update the Hash table with it's new location 
		    gameStateMap.put(destination, currentSelectedChess);
		}
			
	}	
	
	
	public void SaveGame(int time_remaining)
    {
        
	    GUIController guiCtrl = gameCtrl.getGUICtrl();
	    PlayerController pCtrl = gameCtrl.getPlayerController();
	            
        JFileChooser save_window = new JFileChooser();
        File save_file = null;
    
        
        if (save_window.showSaveDialog(save_window) == JFileChooser.APPROVE_OPTION)
        {
        
        try
          {
            save_file = save_window.getSelectedFile();
            FileOutputStream fileOut = new FileOutputStream(save_file); 
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
             //Save the hash map
             out.writeObject(getMap());
             //Save the Current Player Name
             out.writeObject(pCtrl.getCurrentPlayer().getName());
             //Save the current player's points
             out.writeObject(pCtrl.getCurrentPlayer().getPoint());
             //Save the Current Player Name
             out.writeObject(pCtrl.getOtherPlayer().getName());
             //Save the other player's points
             out.writeObject(pCtrl.getOtherPlayer().getPoint());
             //Save the original countdown timer time
             out.writeObject(guiCtrl.getTimer().getTurnTime());
             //Save the time remaining
             out.writeObject(time_remaining);
             
             out.close();
             fileOut.close();
             guiCtrl.showError("Game Successfully Saved");
          }
        
            catch(Exception e)
          {
                guiCtrl.showError(e.toString());
                guiCtrl.showError("Error Saving Game. Please check the file and try again");
                return;
          }
        }
        else
        {
            guiCtrl.showError("Game Not Saved");
        }
    }
	
	
	public boolean LoadGame() 
    {
	    
	    GUIController guiCtrl = gameCtrl.getGUICtrl();
        PlayerController pCtrl = gameCtrl.getPlayerController();
        
        HashMap<String, ChessPiece> loaded_game_state = null;
        String loaded_current_player_name = "";
        int loaded_current_player_points = 0;
        String loaded_other_player_name = "";
        int loaded_other_player_points = 0;
        int loaded_timer_original_time = 0;
        int loaded_time_remaining = 0;
        JFileChooser load_window = new JFileChooser();
        File load_file = null;
        
        if (load_window.showOpenDialog(load_window) == JFileChooser.APPROVE_OPTION)
        {
            
            try
            {
                load_file = load_window.getSelectedFile();
                FileInputStream fileIn = new FileInputStream(load_file);
                ObjectInputStream in = new ObjectInputStream(fileIn);
                loaded_game_state = (HashMap<String, ChessPiece>) in.readObject();
                loaded_current_player_name = (String) in.readObject();
                loaded_current_player_points = (int) in.readObject();
                loaded_other_player_name = (String) in.readObject();
                loaded_other_player_points = (int) in.readObject();
                loaded_timer_original_time = (int)in.readObject();
                loaded_time_remaining = (int)in.readObject();
           
                in.close();
                fileIn.close();
             
                guiCtrl.showError("Game Successfully Loaded");
                
            }
            catch(Exception i)
            {
                guiCtrl.showError("Error with Game load. Please check the file and try again");
                return false;
            }
         
          //Load the Hashmap
            
    
            
            //Clear the current Hash map
            getMap().clear();
            
            //Clear the old game board
            guiCtrl.endGame();
            
            //Add the loaded hash map elements to the existing hash map.
            Iterator itr = loaded_game_state.entrySet().iterator();
            while (itr.hasNext()) 
            {
                Map.Entry pair = (Map.Entry)itr.next();
                getMap().put((String)pair.getKey(),(ChessPiece)pair.getValue());
            }
            
            //Load the Hash map elements into the gameboard
            guiCtrl.initBoard(loaded_game_state);
          
            //Check that the player array exitsts in the game , if not ,create it.
            //Used when the user directly loads a game without starting a new game
            if ( pCtrl.getCurrentPlayer() == null)
            {
                pCtrl.createPlayers("black","white");
            }
            
            
            //Update the points in game
            pCtrl.UpdatePlayerPoint(loaded_current_player_name, loaded_current_player_points);
            pCtrl.UpdatePlayerPoint(loaded_other_player_name, loaded_other_player_points);
            
            
            //Set the label for the both player's points
            guiCtrl.updatePoint(loaded_current_player_name, loaded_current_player_points);
            guiCtrl.updatePoint(loaded_other_player_name, loaded_other_player_points);
            
            // Update the turn in the game
            if (pCtrl.getCurrentPlayer().getName().equalsIgnoreCase(loaded_current_player_name) == false)
            {
                pCtrl.changeTurn();
            }
            //Set the label for the current player
            guiCtrl.setPlayerTurn(loaded_current_player_name);
            
    
            //Update the time remaining 
            GameTimer.resetTimer(loaded_time_remaining);
            
            //Update the orginal turn time from the loaded file
            guiCtrl.getTimer().setLoadedTurnTime(loaded_timer_original_time);
            
            return true;
          
        }
        else
        {
        guiCtrl.showError("Game not loaded");
        return false;
        }
        
    }
	
	
	public HashMap<String, ChessPiece> getMap(){
	    return gameStateMap;
	}
	
	
	public String getSquare(ChessPiece chess){
	    String square = "";
	    
	    for (Map.Entry<String, ChessPiece> e : gameStateMap.entrySet()) {
	        
	    	if ( e.getValue().equals(chess))
	    	{
	    		square = e.getKey();
	    		
	    	}	
	    }
	    
	    return square;
	}
	
	//Used to make the colors proper after the split
    public void UpdateColorsInHashMap() 
    {
        HashMap<String, ChessPiece> map_to_check = getMap();
        
        Iterator it = map_to_check.entrySet().iterator();
        while (it.hasNext()) 
        {
            Map.Entry pair = (Map.Entry)it.next();
            ChessPiece p = (ChessPiece) pair.getValue();
            if (p.getColor().length() > 1)
            {
                p.setProperties(p.getName(), p.getColor().substring(0, 1), p.getSquareColor());
                
                //to_return = to_return +  pair.getKey() + " = " + p.getName() +'\n';
            }
        }
        
        //to_return = addBreak(to_return);

    }
	
	
}
