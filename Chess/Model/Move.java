/* File Name : Game.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * File created by Cynthia
 */
package Model;

import java.io.Serializable;
import java.util.ArrayList;

public interface Move extends Serializable {
    
    ArrayList<String> suggestMoves(int startx, int starty, int[] limit);

}
