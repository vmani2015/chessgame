/* File Name : ChessPiece.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * SH : File creator
 */
package Model;

import java.util.ArrayList;

public interface ChessPiece extends Cloneable{
    
    public Object clone() throws CloneNotSupportedException;
    public void setProperties(String name, String color, String squareColor);
    public String getColor();
    public String getName();
    public String getSquareColor();
    public int getPoint();
    public ArrayList<Move> getMoves();
    public ArrayList<String> getSuggestMoves(int startx, int starty, int[] limits);

}
