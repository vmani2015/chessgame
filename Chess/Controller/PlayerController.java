/* File name : PlayerController.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 * Vinayak MANI - (S3496268)
 *  
 * File created by Cynthia
 * VM : Added getPlayerwithHighestPoints() method.
 */

package Controller;

import java.io.Serializable;

import Model.Player;


public class PlayerController implements Serializable {
    
	private Player[] players;
    private Player currentPlayer;
    
    public void createPlayers(String p1, String p2)
    {
        this.players = new Player[] { new Player(p1), new Player(p2) };
        currentPlayer = players[1];
    }
    
    public Player getCurrentPlayer()
    {
        return currentPlayer;
    }
    
    public Player getOtherPlayer()
    {
        
        Player player = null;
        
        if (currentPlayer.getName() == players[1].getName())
            player = players[0];
        else
            player = players[1];
        
        return player;
    }
    
    public void changeTurn()
    {
        if (currentPlayer.getName() == players[1].getName())
            currentPlayer = players[0];
        else
            currentPlayer = players[1];

    }
    
    public boolean isCurrentPlayer(String color)
    {
        if (currentPlayer.getName() == color)
            return true;
        
        else
            return false;
    }
    
    public String getPlayerwithHighestPoints()
    {
    	
    	if (players[1].getPoint() > players[0].getPoint())
    		return players[1].getName();

    	else if (players[0].getPoint() > players[1].getPoint())
    		return players[0].getName();

    	else
    		return "Draw";
    	
    }
    
    //Needed for Load Game
    public void UpdatePlayerPoint(String pname, int point)
    {
    	for (Player p : players)
    	{
    		if (p.getName().equalsIgnoreCase(pname))
    		{
    			p.setPoint(point);
    		}
    	}
    }
    
    
//    public String getPlayerPointsString()
//    {
//        String to_return  = "";
//        
//        //Get the string for 1 player.
//        to_return = currentPlayer.getName();
//        to_return = to_return+":";
//        to_return = to_return+ currentPlayer.getPoint();
//        
//        to_return = to_return +'\n';
//        
//        //Get the string for second player
//        to_return = to_return + getOtherPlayer().getName();
//        to_return = to_return+":";
//        to_return = to_return+ getOtherPlayer().getPoint();
//        
//        to_return = to_return +'\n';
//        
//        //to_return = addBreak(to_return);
//        
//        return to_return;
//    }
    
    
    
    

}
