/* File Name : GUIController.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 * Vinayak MANI - (S3496268)
 *  
 * File created by Cynthia
 * VM : Added Timer functions, Timer Listener class
 */

package Controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.swing.JOptionPane;

import Model.ChessPiece;
import Model.GameTimer;
import View.BoardGUI;
import View.SplitChessMenuGUI;
import View.GameSettingsGUI;
import View.MainGUI;
import View.MenuGUI;

public class GUIController implements Serializable {
	
	private GameController gameCtrl;
	private final MainGUI gui;
	private GameTimer gameTimer;
	private int[] boardSize;
	private int defaultBoardSize;
	private SplitChessMenuGUI chessPieceSplitMenu;
	
	public GUIController(GameController gameController,int defaultBoardSize) {
	    this.gameCtrl = gameController;
	    this.defaultBoardSize = defaultBoardSize;
	    this.boardSize = new int[]{defaultBoardSize,defaultBoardSize};
		this.gui = new MainGUI(this, boardSize);
		gui.setVisible(true);
		this.gameTimer = new GameTimer(this);
		this.chessPieceSplitMenu = new SplitChessMenuGUI(this);
	}

	public int[] getBoardSize(){
		return this.boardSize;
	}
	
	public String getBgColor(int row, int col){
        return getBoard().getBgColor(row, col);
    }
    
	// Called by GameController
    public void displaySuggestive(Boolean display, int[] square){
        if (display)
            getBoard().showSuggestive(square[0], square[1]);
        else
            getBoard().resetBgColor();
    }

	
	public void initBoard(HashMap<String, ChessPiece> gameState){
		
		Iterator itr = gameState.entrySet().iterator();
	    
	    Map<String, ChessPiece> map = gameState;
	    
	    while (itr.hasNext()) {
	    	
	        Map.Entry pair = (Map.Entry)itr.next();
	        
	        String squareName = ((String) pair.getKey());
	        String name = ((ChessPiece) pair.getValue()).getName();
	        String color = ((ChessPiece) pair.getValue()).getColor();
	        
	        int[] squareNo = gameCtrl.getGameBoard().getSquareNo(squareName);
	        
	        String imageName = "";
	        
	        
	        if (name.equalsIgnoreCase("barrier") == false)
	        	imageName = color + "_" + name;

	        else
	        	imageName = name;
	        
	        getBoard().fillSquare(squareNo[0], squareNo[1], imageName);
	        
	    }
			
	}
	
	public void endGame(){
		
		//We need to clear the squares after the user clicks this
		getBoard().resetBgColor();
		
		//Make the timer stop here
		GameTimer.stopTimer();
		
		//If there was a timer value loaded in , reset it to 0
		//Needed for a new game
		if (gameTimer.getLoadedTurnTime() != 0)
		{
			gameTimer.setLoadedTurnTime(0);
		}
		
		//Reset the label.
        gui.updateTimeLabel("00 : 00 : 00");
        gui.updatePlayerTurnLabel(" - ");
		
		//Remove all chess pieces from the game board
		
		int colSize = boardSize[0];
		int rowSize = boardSize[1];
		
		for (int row = 0; row < rowSize; row++)
		{
		    for (int col = 0; col < colSize; col++)
		        getBoard().unfillSquare(row, col);
		}
		
		//reset score
		gui.getMenu().setWhiteScore(0);
		gui.getMenu().setBlackScore(0);
		
	}
	
	public void showError(String msg){
		JOptionPane.showMessageDialog(null, msg);
	}
	
	
	public void updateSquare(int[] newSquare, int[] oldSquare, String image){
		getBoard().fillSquare(newSquare[0], newSquare[1], image);
		getBoard().unfillSquare(oldSquare[0], oldSquare[1]);
		getBoard().resetBgColor();
		
		//Make the timer stop here
        GameTimer.stopTimer(); //timertouse.stop();
        //Used for a loaded game
        if ( gameTimer.getLoadedTurnTime() != 0)
        {
        	GameTimer.resetTimer(gameTimer.getLoadedTurnTime());
        }
        else
        {
        	GameTimer.resetTimer(gameTimer.getTurnTime());
        }
        
        GameTimer.startTimer(); //timertouse.start();
        
      //Set the turn label
        gui.updatePlayerTurnLabel(gameCtrl.getPlayerController().getOtherPlayer().getName());
	}
	
	
	
	public SplitChessMenuGUI getSplitMenu(){
	    return this.chessPieceSplitMenu;
	}
	
	
	public void setSplitInGUI(Boolean split, String[] splitMsgs){
	    if(split)
	        chessPieceSplitMenu.enableSplit(splitMsgs);
	    else
	        chessPieceSplitMenu.disableSplit();
	}

	
	//Called by GameController
	public void updatePoint(String player, int point){
	    gui.updatePoint(player, point);
	}
	
	//Called by the GameController
    public void setPlayerTurn(String pturn)
    {
        gui.updatePlayerTurnLabel(pturn);
    }
	
	//Called by the GameController
	public GameTimer getTimer()
	{
		return this.gameTimer;
	}
	
	
	//Used to work out the color of the split menu items.
	public GameController getGameController()
	{
		return gameCtrl;
	}
	
		
	public boolean displayGameSettingsPrompt() {
	    
	    GameSettingsGUI settingMenu = new GameSettingsGUI(defaultBoardSize);

	    return settingMenu.displayMenu(gameTimer.getTurnTime(), boardSize, this);
	}
	
	public void confirmGameSettings(int input_time, int rows, int cols, int totalPieces){
	    
        gameTimer.setLoadedTurnTime(input_time);
        
        //Set new board size
        boardSize[0] = rows;
        boardSize[1] = cols;
        
        gameCtrl.customGameSettings(boardSize, totalPieces);
        gui.refreshFrame(boardSize);
	}


    public BoardGUI getBoard() {
        return gui.getBoard();
    }


    public class MenuListener implements ActionListener{
		
		MenuGUI menu;
		
		public MenuListener(MenuGUI menu){
			this.menu = menu;
		}
		
		public void actionPerformed(ActionEvent e) {
		
			String action = e.getActionCommand();
		
			if (action.equals("Quit")) {
				System.exit(0); 
			}
			else if (action.equals("New")) 
			{
				
			    boolean selectedBlack = displayGameSettingsPrompt();

				initBoard(gameCtrl.newGame());
				
				MenuGUI.newGame.setEnabled(false);
				MenuGUI.endGame.setEnabled(true);
				MenuGUI.saveGame.setEnabled(true);
//				MenuGUI.joinGame.setEnabled(false);
//				MenuGUI.hostGame.setEnabled(false);
				
				//Set the turn according to the player choice
				//As the default is white goes first, we only need to process black
				if (selectedBlack)
				{
					gameCtrl.getPlayerController().changeTurn();
				}
				
				//Start the timer here
				GameTimer.startTimer();
				
				//Set the turn label
                gui.updatePlayerTurnLabel(gameCtrl.getPlayerController().getCurrentPlayer().getName());
                
			}
			else if (action.equals("End")) 
			{
				gameCtrl.endGame();
				endGame();
				MenuGUI.newGame.setEnabled(true);
				MenuGUI.endGame.setEnabled(false);
				MenuGUI.saveGame.setEnabled(false);
//				MenuGUI.joinGame.setEnabled(true);
//                MenuGUI.hostGame.setEnabled(true);
			}
			else if ( action.equals("Save"))
			{
				
				int hours_to_seconds;
				int minutes_to_seconds;
				int seconds;
				int time_remaining;
				
				GameTimer.stopTimer();
				String time = this.menu.getTimeLabel().getText();
				time.trim();
				String time_array[] = time.split(" :");
				hours_to_seconds = Integer.parseInt(time_array[0].trim())*3600;
				minutes_to_seconds = Integer.parseInt(time_array[1].trim())*60;
				seconds = Integer.parseInt(time_array[2].trim());
				time_remaining = hours_to_seconds + minutes_to_seconds + seconds;
				gameCtrl.getGame().SaveGame(time_remaining);
				GameTimer.startTimer();
			}
			
			else if (action.equals("Load"))
			{
				boolean load_success = false;
				
				GameTimer.stopTimer();
				load_success = gameCtrl.getGame().LoadGame();
				
				//Only enable the buttons for save game and new game if the load was successful
				
				if (load_success)
				{	
					menu.saveGame.setEnabled(true);
					menu.endGame.setEnabled(true);	
				}	
				//If there is a game in progress and load fails
				//Start the timer again in this case
				if (menu.saveGame.isEnabled() == true)
				{
					GameTimer.startTimer();
				}
				
				
			}
		}
	}
	
	public class BoardListener implements ActionListener{
		
		BoardGUI board;
		
		public BoardListener(BoardGUI board){
			this.board = board;
		}
		public void actionPerformed(ActionEvent e) {
			String action = e.getActionCommand();
			gameCtrl.checkSelected(action);
		}
	}
	
	
    public class TimerListener implements ActionListener
    {
        int toCountdown;

        int original_count;


        public TimerListener(int i) {

            this.toCountdown = i;

            this.original_count = i;

        }



        @Override

        public void actionPerformed(ActionEvent arg0) 
        {
            //gui.updateTimeLabel(Integer.toString(toCountdown));
            gui.updateTimeLabel(gameTimer.getDurationString(toCountdown));


            if (toCountdown == 0)
            {
                GameTimer.stopTimer();
                //timertouse.stop();

                showError(" You were too slow! It is the next player's turn");

                getBoard().resetBgColor();

                //Needed for load - reset the timer to the loaded value. Not the original one.
                if (gameTimer.getLoadedTurnTime() != 0)
                {
                	GameTimer.resetTimer(gameTimer.getLoadedTurnTime());
                }
                else
                {
                	GameTimer.resetTimer(gameTimer.getTurnTime());
                }
                
                
              //Change the turn of the player
                gameCtrl.getPlayerController().changeTurn();
                
              //Set the turn label
                gui.updatePlayerTurnLabel(gameCtrl.getPlayerController().getCurrentPlayer().getName());
                
                //Start the timer
                GameTimer.startTimer();
               
            }
            else
            {

                toCountdown = toCountdown - 1;
            }


        }

    }

}
