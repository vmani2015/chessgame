/* File name : GameController.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 * Vinayak MANI - (S3496268)
 * 
 * VM : Defined game logics 
 * SH : Refactor codes, splitting up codes into smaller methods, 
 *      Added createGame(), newGame()
 * VM : Added "check end game" function, getPlayerController(), 
 *      amended isCurrentPlayer() method
 * SH : Added customGameSettings() methods
 */

package Controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

import Model.Board;
import Model.ChessPiece;
import Model.ChessPiecesManager;
import Model.GameEngine;
import Model.GameState;

public class GameController implements Serializable {
	
	private Board gameBoard;
	private GUIController guiCtrl;
	private PlayerController pCtrl;
	private GameEngine gameEngine;
	private MovesController mCtrl;
	private GameState game;
	private ChessPiecesManager cpMgr;
	
	private ChessPiece currentSelectedChess = null;	
	private String origin = null;
	private ChessPiece[] splittedChess = null;
	private final int defaultBoardSize = 6;
	private int totalPieces = 6;
	
	public void createGame(){
	    this.guiCtrl = new GUIController(this, defaultBoardSize);
	    this.gameBoard = new Board(defaultBoardSize);
        this.mCtrl = new MovesController(this);
        this.gameEngine = new GameEngine(this);
        this.pCtrl = new PlayerController();
        this.cpMgr = new ChessPiecesManager(mCtrl);
        this.game = new GameState(cpMgr, this);
	}
	
	
	public HashMap<String, ChessPiece> newGame(){
	    String black = "black";
	    String white = "white";
	    pCtrl.createPlayers(black, white);
	    gameBoard.createBoard();
        return game.newGame(totalPieces, gameBoard, black, white);
	}
	
	
	public void endGame(){
	    currentSelectedChess = null;
        origin = null;
		
		//Declare the winner/draw of the game
        if (pCtrl.getPlayerwithHighestPoints() == "Draw" )
        {
        	guiCtrl.showError("The game has ended and it is a draw");
        }
        else
        {
        	guiCtrl.showError("The game has ended and "+pCtrl.getPlayerwithHighestPoints()+" is the winner");
        }

        game.endGame();
		guiCtrl.endGame();
	}
	
	
	    
	private boolean isCurrentPlayer(String destination){
	    
		//Make sure that the chess piece at the destination is not null
		if (game.getChessPiece(destination) == null)
		{
			guiCtrl.showError("Invalid move. Please click a square with a chess piece on it");
            return false;
		}
		//Only then assign the chessColor and the playerColor
        String chessColor = game.getChessPiece(destination).getColor();        
        String playerColor = pCtrl.getCurrentPlayer().getName();
        
        if (chessColor.equals("") || chessColor.equals(playerColor.substring(0, 1)))
            return true;
        
        else {
            guiCtrl.showError("Invalid. It's " + playerColor + "'s turn.");
            return false;
        }
	}
	
	// To determine an action after player clicked on the board
	// Called by GUIController    
	public void checkSelected(String destination){    
        
	    // Player selecting a piece the first time
        if (currentSelectedChess == null)
        {    
            if(isCurrentPlayer(destination))
            {
                setCurrentChess(game.getChessPiece(destination));
                checkMoveAndShowSuggest(destination);
            } 
        }
        // Pre-selected a chess piece
        else 
        {
            if ( gameEngine.validSecondClick(origin, destination, currentSelectedChess) )
            {
                
                movePieceAndUpdateGUI(destination);
                
                //Check if the game is over
                if ( gameEngine.isGameOver(currentSelectedChess) != "N")
                	this.endGame();
                	
                else
                {
                	pCtrl.changeTurn();
                	guiCtrl.setSplitInGUI(false, null);
                }	
            }
            // Invalid move selected
            else 
            {
                guiCtrl.showError("Invalid move. Please try again");
                guiCtrl.setSplitInGUI(false, null);
            }

            guiCtrl.displaySuggestive(false, null);
            currentSelectedChess = null;
            splittedChess = null;
            origin = null;
        }

    }
	
	private void checkMoveAndShowSuggest(String destination){
        
        if(gameEngine.validFirstClick(destination, currentSelectedChess))
        {
            displaySuggestMove(destination, currentSelectedChess);
        
            origin = destination;
         
            String name = currentSelectedChess.getName();
            String[] splitMsgs = cpMgr.checkIsSplittable(name);
         
            if (splitMsgs != null)
                guiCtrl.setSplitInGUI(true, splitMsgs);

        }
        else{
            String errorMsg = gameEngine.getErrorMsg();
            guiCtrl.showError(errorMsg);
            currentSelectedChess = null;
        }
        
    }
	
	
	
	private void movePieceAndUpdateGUI(String destination){
		
	    if (splittedChess != null)
        {
	    	ChessPiece cdest = game.getChessPiece(destination);
	    	ChessPiece corig = splittedChess[0];
	    	ChessPiece new_cp = null;
	    	
	    	//Need to check if we are merging when splitting
	    	if ( cdest != null)
	    	{
	    		//This is a merge
	    		if (cdest.getColor().equalsIgnoreCase(corig.getColor()))
	    		{
	    			new_cp = cpMgr.merge(cdest, corig);
	    			game.movePiece(origin, destination, new_cp);
	    			updateGUI(destination, origin, new_cp);
	    			
	    		}
	    		//This is a capture
	    		else
	    		{
	    			game.movePiece(origin, destination, splittedChess[0]);
	                updateGUI(destination, origin, splittedChess[0]);
	    		}
	    	}
	    	//This is a move 
	    	else
	    	{
	    		game.movePiece(origin, destination, splittedChess[0]);
                updateGUI(destination, origin, splittedChess[0]);
	    	}
            
            //Code for original chess piece to be where it is .
            game.movePiece(null, origin, splittedChess[1]);
            updateGUI(origin, null, splittedChess[1]);

	    	game.UpdateColorsInHashMap();
            
            splittedChess = null;
           
        }
        else
        {
            game.movePiece(origin, destination, currentSelectedChess);
            updateGUI(destination, origin, currentSelectedChess);
        }
	    
	}
	

	private void displaySuggestMove(String destination, ChessPiece selectedChess){
	    
	    ArrayList<String> suggestedMoves = gameEngine.suggestMove(destination, selectedChess);
        
        // inform GUI to display yellow background on selected square
         int[] squareNo = gameBoard.getSquareNo(destination);
         guiCtrl.displaySuggestive(true, squareNo);
         
         //Remove the suggestions that indicate an invalid merge
         suggestedMoves = gameEngine.RemoveInvalidMergeSuggestions(selectedChess,suggestedMoves);
       
         //GUI to display yellow background on all suggested squares
         for (String move: suggestedMoves)
         {
             squareNo = gameBoard.getSquareNo(move);
             guiCtrl.displaySuggestive(true, squareNo);
         }
	}
	

	private void updateGUI(String destination, String origin, ChessPiece selectedChess)
    {
        
        int[] newSquare = gameBoard.getSquareNo(destination);
        int[] oriSquare = gameBoard.getSquareNo(origin);
        String color = "";   
        String chessImage = selectedChess.getName();
        
        //Needed for the split logic to work
        if (selectedChess.getColor().length() > 1 )
        {
        	color = selectedChess.getColor().substring(0, 1);
        }
        else
        {
        	color = selectedChess.getColor();
        }
        
        String image = color + "_" + chessImage;
        guiCtrl.updateSquare(newSquare, oriSquare, image);
    }
	
	
    // Method to check selected pieces against the hashmap in Game class
    // Called by GUIController class
	public void checkSplit(String chess){
	    
        splittedChess = cpMgr.split(currentSelectedChess, chess);
         
        guiCtrl.displaySuggestive(false, null);
        
        String destination = "";
        
        //Make sure that the first name is equal to one letter
        if (chess.length() >=4)
        {
        	chess = chess.substring(0, 1);
        }
        
        if (chess.equals(splittedChess[1].getName().substring(0, 1)))
        {
            ChessPiece temp = splittedChess[0];
            splittedChess[0] = splittedChess[1];
            splittedChess[1] = temp;  
        }
        
        destination = game.getSquare(currentSelectedChess);
        displaySuggestMove(destination, splittedChess[0]);
	}
	
	public Board getGameBoard() {
		return gameBoard;
	}
	
	public GameState getGame() {
        return game;
    }
	
	public ChessPiecesManager getCPManager(){
	    return this.cpMgr;
	}
	
	public void setCurrentChess(ChessPiece chess){
	    this.currentSelectedChess = chess;
	}
	
	public GUIController getGUICtrl(){
	    return this.guiCtrl;
	}

	public PlayerController getPlayerController(){
	    return this.pCtrl;
	}
	
	public void customGameSettings(int[] boardSize, int totalPieces){
	    gameBoard.setBoardSize(boardSize);
	    this.totalPieces = totalPieces;
	}

}