/* File name : MovesController.java
 * 
 * Author(s):
 * Sin Hsia, KWOK (s3482370)
 *  
 * File created by Cynthia
 * VH : Added isEmptySquare() method
 */

package Controller;

import java.io.Serializable;

import Model.MoveDiagonal;
import Model.MoveLShape;
import Model.MoveStraight;

public class MovesController implements Serializable {
    
    private GameController gCtrl;
    private MoveStraight moveStraight;
    private MoveDiagonal moveDiagonal;
    private MoveLShape moveLShape;
    
    
    public MovesController(GameController ctrl){
        this.moveStraight = new MoveStraight(this);
        this.moveDiagonal = new MoveDiagonal(this);
        this.moveLShape = new MoveLShape(this);
        this.gCtrl = ctrl;
    }
    
    public MoveStraight getMoveS(){
        return moveStraight;
    }
    
    public MoveDiagonal getMoveD(){
        return moveDiagonal;
    }
    
    public MoveLShape getMoveL(){
        return moveLShape;
    }
    
    
    public boolean isEmptySquare(int row, int col)
    {
        String square = gCtrl.getGameBoard().getSquareName(row,col);
        
        if (gCtrl.getGame().getChessPiece(square) == null)
            return true;
        else
            return false;
    }

}