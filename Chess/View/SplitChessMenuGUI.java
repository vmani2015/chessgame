/* File name : SplitChessMenuGUI.java
 * 
 * Author(s):
 * Vinayak MANI - (S3496268)
 * Sin Hsia, KWOK (s3482370)
 *  
 * VM : Author of all methods
 * SH : Extracted codes from GUIController to this new class
 * 
 */
package View;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.ImageIcon;
import javax.swing.JMenuItem;

import Controller.GUIController;

public class SplitChessMenuGUI implements Serializable{
    private boolean splitEnabled;
    private String[] splitMsgs;
    private GUIController guiCtrl;
    
    public SplitChessMenuGUI(GUIController guiCtrl) {
        this.guiCtrl = guiCtrl;
        disableSplit();
    }
    
    // Methods for splitting merged chess
    public boolean getSplitEnabled(){
        return splitEnabled;
    }
    
    public void enableSplit(String[] splitMsgs){
        this.splitEnabled = true;
        this.splitMsgs = splitMsgs;
    }
    
    //Called by GUIController
    public void disableSplit(){
        this.splitEnabled = false;
    }

    
    public String[] getSplitChessNames(){
        return splitMsgs;
    }
    
    //Used for splitting composite pieces
    public String WorkOutSplitName(String input)
    {
        input = input.substring(6); // To remove "Split " from the string
        String output = "";
        
        
        if (input.indexOf("Bishop") > -1)
        {
            if (output.equals(""))
                output = "b";
            else
                output = output + "b";
        }
        
        if (input.indexOf("Knight") > -1)
        {
            if (output.equals(""))
                output = "k";
            else
                output = output +  "k";
        }
        
        if (input.indexOf("Rook") > -1)
        {
            if (output.equals(""))
                output = "r";

            else
                output = output + "r";

        }
        
        return output;
    }
    
    //Called by BoardGUI
    public void splitRequest(String chess){
        guiCtrl.getGameController().checkSplit(chess);
    }
    
    public void showSplitMenu(String icon_color){
        BoardGUI boardGui = guiCtrl.getBoard();
        String[] chessNames = getSplitChessNames();

        ActionListener menuListener = new ActionListener() {
          public void actionPerformed(ActionEvent event) {

            String chess = event.getActionCommand().toString();
            chess = WorkOutSplitName(chess);
            //For single pieces
            if (chess.equals("r"))
                splitRequest("rook");
            
            else if (chess.equals("b"))
                splitRequest("bishop");
            
            else if (chess.equals("k"))
                splitRequest("knight");
            
           //For composite pieces
            else if (chess.equals("bk") || chess.equals("kb"))
                splitRequest("bk");

            else if (chess.equals("br") || chess.equals("rb"))
                splitRequest("br");

            else if (chess.equals("kr") || chess.equals("rk"))
                splitRequest("kr");

          }
        };
        
        JMenuItem item;
        
        for (int i = 0; i < chessNames.length; i++){
            
            String icon_name = "";
            icon_color = icon_color.substring(0, 1);
            icon_name = chessNames[i].toLowerCase();
            if ( icon_name.contains("+"))
            {
                String[] icon_names = icon_name.split("\\+");
                String p1 = icon_names[0].trim();
                String p2 = icon_names[1].trim();
                
                p1 = p1.substring(0, 1);
                p2 = p2.substring(0, 1);
                
                icon_name = p1+p2;
            }
            String icon_path = icon_color+"_"+icon_name;
            ImageIcon icon_image = boardGui.getImageIcon(icon_path); 
            
            item = new JMenuItem("Split " + chessNames[i],icon_image);
            item.setIcon(icon_image);
            boardGui.popup.add(item);
            item.setHorizontalTextPosition(JMenuItem.RIGHT);
            item.addActionListener(menuListener);

        }
    }
    

}
