/* File name : GameSettingsGUI.java
 * 
 * Author(s):
 * Vinayak MANI - (S3496268)
 * Sin Hsia, KWOK (s3482370)
 *  
 * VM : Created displayMenu() method and convertStringtoInt() method
 * SH : Extracted all codes from GUIController to this new class
 * SH : Refactored JPanels(timerPanel, boardPanel and chessPanel), 
 *      added codes for getting board size input 
 * 
 */
package View;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Controller.GUIController;
import Model.GameTimer;

public class GameSettingsGUI implements Serializable {
    
    private int defaultBoardSize;
    
    public GameSettingsGUI(int boardSize){
        this.defaultBoardSize = boardSize;
    }
    
    public boolean displayMenu(int defaultTimer, int[] boardSize, GUIController gCtrl){
        
        JLabel instruction = new JLabel("Select 'Start with default settings' to begin ");
        JLabel customise = new JLabel("or customise game settings below: ");
        JLabel separator = new JLabel("===============================================");
        
      //Ask the user for timer values before we continue with this code.
        JPanel timerPanel = new JPanel(new BorderLayout());
       
        JLabel title_label = new JLabel(" 1. Please enter the game turn duration");
        JTextField hours = new JTextField(3);
        hours.setText("0");
        JTextField minutes = new JTextField(3);
        minutes.setText("0");
        JTextField seconds = new JTextField(3);
        seconds.setText(String.valueOf(defaultTimer));
        
        timerPanel.add(title_label, BorderLayout.NORTH);
        
        // Timer customisation
        JPanel options = new JPanel(new FlowLayout(FlowLayout.LEFT));
        options.add(new JLabel("Hours: "));
        options.add(hours);
        options.add(new JLabel("  Minutes:"));
        options.add(minutes);
        options.add(new JLabel("  Seconds:"));
        options.add(seconds);
        
        timerPanel.add(options, BorderLayout.CENTER);
        
        // Player turn's customization
        JPanel turn = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JLabel start_label = new JLabel("2. Who starts first?");
        final JRadioButton white = new JRadioButton("White");
        white.setSelected(true);
        final JRadioButton black = new JRadioButton("Black");
        black.setSelected(false);
        
        turn.add(start_label);
        turn.add(white);
        turn.add(black);
        
        //board customization
        JPanel boardPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel boardSizePanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        
        JTextField rows = new JTextField(3);
        rows.setText(String.valueOf(defaultBoardSize));
        JTextField cols = new JTextField(3);
        cols.setText(String.valueOf(defaultBoardSize));
        
        boardSizePanel.add(new JLabel("Rows:"));
        boardSizePanel.add(cols);
        boardSizePanel.add(new JLabel("Columns:"));
        boardSizePanel.add(rows);

        boardPanel.add(new JLabel("3. Enter board size:  "));
        boardPanel.add(boardSizePanel);
        
        final JTextField totalNo = new JTextField(3);
        totalNo.setText("0");
        
        //Chess pieces customization
        JPanel chessPanel = new JPanel(new BorderLayout());
        JPanel detailPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        final JCheckBox defaultNo = new JCheckBox("Fill the entire row");
//        defaultNo.setSelected(true);
        defaultNo.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				if (defaultNo.isSelected())
				{
					totalNo.setText("N/A");
					totalNo.setEnabled(false);
				}
				else
				{
					totalNo.setText("0");
					totalNo.setEnabled(true);
				}
				
			}
		});
      
        
        
        
        
       
        
        chessPanel.add(new JLabel(" 4. Please select total number of Chess pieces:"), BorderLayout.NORTH);
        chessPanel.add(defaultNo, BorderLayout.CENTER);
        detailPanel.add(new JLabel("Only fill"));
        detailPanel.add(totalNo);
        detailPanel.add(new JLabel(" pieces in one row"));
        chessPanel.add(detailPanel, BorderLayout.SOUTH);
        
       
        class DisableBlack implements ActionListener
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                black.setSelected(false);
                
            }
        }
        
        class DisableWhite implements ActionListener
        {
            @Override
            public void actionPerformed(ActionEvent e) {
                white.setSelected(false);
            }
        }
        
        white.addActionListener(new DisableBlack());
        black.addActionListener(new DisableWhite());
        
        Object[] message = {
                instruction, customise, separator, 
                timerPanel,
                turn, 
                boardPanel, new JLabel(" "),
                chessPanel, new JLabel(" ")};
        
        int input_time = -1;
        int hour = 0;
        int minute = 0;
        int second = 0;
        
        
        while ( input_time == -1 || (black.isSelected() == false && white.isSelected() == false ))
        {

            int option = JOptionPane.showOptionDialog(null, 
                    message, 
                    "Start New Game", 
                    JOptionPane.OK_CANCEL_OPTION, 
                    JOptionPane.INFORMATION_MESSAGE, 
                    null, 
                    new String[]{"Start with new settings", "Start with default settings"},
                    "default");
            
            int hrs = convertStringtoInt(hours.getText());
            int min = convertStringtoInt(minutes.getText());
            int sec = convertStringtoInt(seconds.getText());
            
            int[] defaultSizes = {defaultBoardSize, defaultBoardSize};
            
            if (option == JOptionPane.OK_OPTION) 
            {
                if (hrs < 0 )
                {
                    JOptionPane.showMessageDialog(null, "Please enter a valid hour number greater than 0");
                }
                else if (min < 0 || min >= 60)
                {
                    JOptionPane.showMessageDialog(null, "Please enter a valid minute number greater than 0 and less than 60");
                }
                else if (sec < 0 || sec >= 60 )
                {
                    JOptionPane.showMessageDialog(null, "Please enter a seconds number greater than 0 and less than 60");
                }
                else if (hrs == 0 && min == 0 && sec == 0 )
                {
                    JOptionPane.showMessageDialog(null, "A turn must be at least 1 second");
                    return true;
                }
                
                else if (black.isSelected() == false && white.isSelected() == false )
                {
                    JOptionPane.showMessageDialog(null, "Please select a player to go first");
                }
                else if(Integer.valueOf(rows.getText()) > 26 
                        || Integer.valueOf(cols.getText()) > 26)
                {
                    JOptionPane.showMessageDialog(null, "The Size must be less than " +
                            26 + " for row, and "+ 26 + " for columns");
                }
                
                else if(Integer.valueOf(rows.getText()) < 5 
                        || Integer.valueOf(cols.getText()) < defaultSizes[0])
                {
                    JOptionPane.showMessageDialog(null, "Size must be greater than " +
                            defaultSizes[0] + " for row, and "+ 5 + " for columns");
                }
                
                else if(defaultNo.isSelected() == false && Integer.valueOf(totalNo.getText()) < 1){ 
                        JOptionPane.showMessageDialog(null, "Please enter the number of pieces to be added");
                }
                
                else if(defaultNo.isSelected() == false &&
                        Integer.valueOf(totalNo.getText()) > Integer.valueOf(cols.getText()))
                {
                        JOptionPane.showMessageDialog(null, "Please enter a number less than the board sized " 
                                + Integer.valueOf(cols.getText()));
                }

                else
                {
                    //Get the values from the text field
                    hour = hrs;
                    minute = min;
                    second = sec;
                    
                    //Convert the values
                    hour = hour*3600;
                    minute = minute * 60;
                    
                    //Set the input time
                    input_time = hour + minute + second;
                    
                    int rowInt = Integer.valueOf(rows.getText());
                    int colInt = Integer.valueOf(cols.getText());
                    
                    GameTimer.resetTimer(input_time);
                    
                    int totalPieces = (defaultNo.isSelected()) ? rowInt : Integer.valueOf(totalNo.getText());
                    
                    gCtrl.confirmGameSettings(input_time, rowInt, colInt, totalPieces);
                    
                }
            }
            else 
            {
                GameTimer.resetTimer(defaultTimer);
   
                gCtrl.confirmGameSettings(defaultTimer, defaultBoardSize, defaultBoardSize, defaultBoardSize);
                
                break;
            }
        }

        return black.isSelected();
    
    }
    
  //Used for timer input
    public int convertStringtoInt(String input)
    {
        int number = -1;
        try
        {
            number = Integer.parseInt(input);
            return number;
            
        }
        catch(Exception a)
        {
            return number;
        }   
    }

}
