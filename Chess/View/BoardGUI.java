/*File Name : BoardGUI.java
 * 
 * Author: Siti Nur Aini ZAHARI
 * Student Number: s3460603
 * This is the GUI class
 * 
 * Updated by Vinayak Mani
 * Student Number : s3496268
 * Included code to enable drag and drop functionality.
 * Inclused code to enable split menu selection
 */

package View;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Robot;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.MouseInputAdapter;

import Controller.GUIController;
import Model.ChessPiece;

public class BoardGUI implements Serializable {
	
	private JPanel boardPanel;
	int colSize;
	int rowSize;
	private JButton[][] boardSquares;
	private final String ROWNAME = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private GUIController gCtrl;
	public JPopupMenu popup;
	private MainGUI mainGui;
	
	//Used for dragging
	private String drag_origin = "";
	private String drag_destination = "";
	boolean has_clicked = false;
	boolean has_released = false;
	
	public BoardGUI(GUIController gCtrl, MainGUI main, int[] defaultBoardSizes){
		this.gCtrl = gCtrl;
		this.mainGui = main;
		this.boardPanel = new JPanel();
		this.rowSize = defaultBoardSizes[1];
		this.colSize = defaultBoardSizes[0];
	}
	
	public JPanel getBoard(){
		return boardPanel;
	}
	
	public JButton[][] getSquares(){
		return boardSquares;
	}

	public void createBoard(){
	    
	    int squareSize = mainGui.getSquareSize();
	    
	    int boardWidth = colSize * squareSize;
	    int boardHeight = rowSize * squareSize;
	    int startRow = 0;
	    int startCol = 0;
	    
	    //Customize boardPanel
	    boardPanel.setLayout(null);
	    boardPanel.setLocation(10, 80);
	    boardPanel.setSize(boardWidth, boardHeight);
	    
	    this.boardSquares = new JButton[rowSize][colSize];
	   
	    
	    for (int row = 0; row < rowSize; row++) 
        {
	        for (int col = 0; col < colSize; col++) 
	        {
	              JButton square = new JButton();
	              square.setSize(squareSize, squareSize);
	              
	              if ((row % 2 == 1 && col % 2 == 1)|| (row % 2 == 0 && col % 2 == 0))
	                  square.setBackground(Color.DARK_GRAY);
	              else
	                  square.setBackground(Color.WHITE);
	              
	              square.setLocation(startRow, startCol);
	              //square.addActionListener(this);
	              boardPanel.add(square);
	              startRow += squareSize;
	              
	              boardSquares[row][col] = square;
	              boardSquares[row][col].setFocusPainted(false);
	              
	              String squareName = ROWNAME.substring(row, row + 1) + (col + 1);
	              boardSquares[row][col].setActionCommand(squareName);    
//	              Normal click code
	              boardSquares[row][col].addActionListener(gCtrl.new BoardListener(this));
	              
	              //Drag and drop code
//	              MouseListener listener = new DragMouseAdapter(squareName,square);
//	              boardSquares[row][col].addMouseListener(listener);
	              
	              square.addMouseListener(new MouseAdapter() {
	                  public void mouseReleased(MouseEvent e){
	                      if ( e.getButton() == 3 ){ // 1-left, 2-middle, 3-right button
	                          if (gCtrl.getSplitMenu().getSplitEnabled() == true){
	                              popup = new JPopupMenu();
	                              String icon_color = gCtrl.getGameController().getPlayerController().getCurrentPlayer().getName();
	                              gCtrl.getSplitMenu().showSplitMenu(icon_color);
	                              popup.show(e.getComponent(), e.getX(), e.getY());
	                          }
	                      }
	                  }
	              });
	          }
	          startRow = 0;
	          startCol += squareSize;
	      }
	}
	
	public void refreshBoard(int[] boardSize){
        this.boardPanel.removeAll();

	    rowSize = boardSize[1];
        colSize = boardSize[0];     
        createBoard();
	}
	
	public void fillSquare(int row, int col, String image){
		ImageIcon icon = getImageIcon(image);
		boardSquares[row][col].setIcon(icon);
	}
	
	public void unfillSquare(int row, int col){
		boardSquares[row][col].setIcon(null);
	}

	public ImageIcon getImageIcon(String image){
		String chess = "images/"+ image + ".png";
		
		return new ImageIcon(chess);
	}
		
	//highlight border of given square
	public void showSuggestive(int row, int col)
	{
		boardSquares[row][col].setBackground(Color.YELLOW);
	}
	
	// set square background color to original state
	public void resetBgColor()
	{
	    for (int row = 0; row < rowSize; row++) 
        {
	        for (int col = 0; col < colSize; col++) 
	        {
				if ((row % 2 == 1 && col % 2 == 1)|| (row % 2 == 0 && col % 2 == 0)) 
					boardSquares[row][col].setBackground(Color.DARK_GRAY);
				else
					boardSquares[row][col].setBackground(Color.WHITE);
			}
		}
	}
	
	public String getBgColor(int row, int col){
		
		Color squareColor = boardSquares[row][col].getBackground();
		
		String color = "";
		if (squareColor == Color.WHITE)
			color = "white";
		else
			color = "black";
				
		return color;
	}
	
	class DragMouseAdapter extends MouseInputAdapter 
	{
  		
		private String Square_Name;
	
  		public DragMouseAdapter(String squareName, JButton psquare) 
  		{
			this.Square_Name = squareName;
		}
  		
  		public String getSquareName()
  		{
  			return this.Square_Name;
  		}

		public void mousePressed(MouseEvent event) 
		{   
          
			if (drag_origin == ""  && event.getButton() == 1)
            {	
			 
				drag_origin = getSquareName();
				ChessPiece temp = gCtrl.getGameController().getGame().getChessPiece(drag_origin);
				
				//If the user clicks an invalid square
				if (temp == null)
				{
					//Then we reset the origin and the destination to be ""
					drag_origin = "";
					drag_destination = "";
					has_clicked = false;
					has_released = false;
				}
				else
				{	
					gCtrl.getGameController().checkSelected(drag_origin);
					has_clicked = true;	
				}
            }
        }
			
		public void mouseReleased(MouseEvent e)
		{
			int testx = e.getXOnScreen();
			int testy = e.getYOnScreen();	
	
			if ( e.getButton() == 1 && has_clicked && has_released == false )
			{
				try 
				{
					//Using a robot to simulate another mouse click on the area 
					//that the user released the mouse on
					Robot r = new Robot();
					r.mouseMove(testx,testy);
					r.mousePress(InputEvent.BUTTON1_MASK);
					r.mouseRelease(InputEvent.BUTTON1_MASK);
					has_released = true;
					r.mouseMove(testx-100, testy);
					//Declare this as null after work in order to free up memory
					r = null;
				} 
				catch (AWTException e1) 
				{
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				
			}
		}
			
		@Override
        public void mouseExited(MouseEvent e)
		{
			if ( has_clicked && has_released )
	    	{	
				drag_destination = getSquareName();
		    	
		    	//Try and validate the second controller
		    	gCtrl.getGameController().checkSelected(drag_destination);
	    		//Then we reset the origin and the destination to be ""
	    		drag_origin = "";
	    		drag_destination = "";
	    		has_clicked = false;
	    		has_released = false;
	    			
			}
		}	
	}
	
	
}
