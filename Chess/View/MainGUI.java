/* File Name : BoardGUI.java
 * 
 * Author: Siti Nur Aini ZAHARI
 * Student Number: s3460603
 * This is the GUI class
 */

package View;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.border.Border;
import javax.swing.plaf.FontUIResource;

import Controller.GUIController;

public class MainGUI extends JFrame  {
	
	private JPanel boardPanel;
	private BoardGUI gameBoard;
	private MenuGUI menu;
	private int squareSize = 100;
	private JFrame frame;
	private int minFrameWidth = 0; 
	private int minFrameHeight = 0;


	public MainGUI(GUIController gCtrl, int[] defaultBoardSizes){

	    super("-= Interactive  Chess =-"); //set title
	    
	    //instantiate Menu on top page
        this.menu = new MenuGUI(gCtrl);
        //instantiate Board
        this.gameBoard = new BoardGUI(gCtrl, this, defaultBoardSizes);
        
        this.frame = setFrame(defaultBoardSizes, this);
        
        Container c = getContentPane();
        
        c.add(menu.createMenuPanel(), BorderLayout.LINE_START);
        c.add(menu.createTitlePanel(), BorderLayout.PAGE_START);
        
        boardPanel = createContentPane();
        c.add(boardPanel, BorderLayout.CENTER);
        
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); //exit completely when program close
        setVisible(true);
	}
	
	public JFrame setFrame(int[] boardSize, JFrame frame){
	    int padding = 10;

        int frameHeight = boardSize[1] * squareSize + squareSize + padding;
        int frameWidth = boardSize[0] * squareSize + squareSize + padding * 4;
        
        if (minFrameHeight == 0 && minFrameHeight == 0) {
            minFrameHeight = frameHeight;
            minFrameWidth = frameWidth;
            frame.setSize(frameWidth,frameHeight); //set frame size
        }
        else if (frameHeight >= minFrameHeight && frameWidth >= minFrameWidth)
            frame.setSize(frameWidth,frameHeight); //set frame size
        
        return frame;
	}
	
	public void refreshFrame(int[] boardSize){
	    
	    frame.getContentPane().remove(boardPanel);
	    
	    JPanel newBoardPanel = createContentPane();
	    frame.add(newBoardPanel, BorderLayout.CENTER);
	    gameBoard.refreshBoard(boardSize);
	    
	    frame = setFrame(boardSize, frame);
        frame.invalidate();
        frame.validate();
        frame.repaint();
	}
	
	public JPanel createContentPane (){

	    boardPanel = new JPanel();
	    boardPanel.setLayout(new BorderLayout());
	  
	    boardPanel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        gameBoard.createBoard();
        
        boardPanel.add(gameBoard.getBoard());
        
        return boardPanel;
	}
	
	public BoardGUI getBoard(){
		return gameBoard;
	}
		
	public void updatePoint(String currentPlayer, int point){
		//Update White or Black point where necessary
	    
	    if(currentPlayer.equalsIgnoreCase("white"))
	        menu.setWhiteScore(point);
	    else
	        menu.setBlackScore(point);            
	}
	
	//Method required for timer
	public void updateTimeLabel(String string)
	{
	    menu.getTimeLabel().setText(string);
	}
	
	public MenuGUI getMenu(){
	    return menu;
	}
	
	public int getSquareSize(){
	    return this.squareSize;
	}

	
	//method required for the turn label
	public void updatePlayerTurnLabel(String string)
	{
	    String label = capitalize(string);
		menu.getPlayerTurnLabel().setText(label);
	}
	
	private String capitalize(String label) {
        return Character.toUpperCase(label.charAt(0)) + label.substring(1);
     }
		
	
}   

