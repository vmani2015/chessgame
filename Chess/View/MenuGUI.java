/*File Name : MenuGUI.java
 * 
 * Author: Siti Nur Aini ZAHARI
 * Student Number: s3460603
 * This is the GUI class
 */

package View;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.io.Serializable;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JToolBar;

import Controller.GUIController;

public class MenuGUI implements Serializable {
	   
	private JToolBar menuBar, titleBar;

	private JLabel whiteScore, blackScore, time, pturn;
	public static JButton newGame, endGame, saveGame, loadGame, hostGame, joinGame; 
	private GUIController gCtrl;

	private JButton quitGame;
	

	public MenuGUI(GUIController gCtrl){
	    this.gCtrl = gCtrl;
	}
	
	public void DeclareJButton(JButton pbutton,String label,JToolBar menu)
	{
		// Declare the JButton
		pbutton = new JButton(label);
		
		// Set focus painted to false- Hide the border when selected
		pbutton.setFocusPainted(false);
		
		// Add the button to the menu
		menuBar.add(pbutton);
	}
	
	public JToolBar createMenuPanel(){
	    
		this.menuBar = new JToolBar(null, JToolBar.VERTICAL);
		menuBar.setFloatable(false);
		menuBar.setLayout(new GridLayout(11,1));
		menuBar.setPreferredSize(new Dimension(100,40));
		
//		DeclareJButton(newGame, "New Game",menuBar);
//		DeclareJButton(endGame, "End Game",menuBar);
//		DeclareJButton(quitGame, "Quit",menuBar);
//		DeclareJButton(saveGame, "Save Game",menuBar);
//		DeclareJButton(loadGame, "Load Game",menuBar);
//		DeclareJButton(hostGame, "Host Game",menuBar);
//		DeclareJButton(joinGame, "Join Game",menuBar);
		newGame  = new JButton("New Game");
		endGame  = new JButton("End Game");
		quitGame = new JButton("Quit");
		saveGame = new JButton("Save Game"); 
		loadGame = new JButton("Load Game");
//		hostGame = new JButton("Host Game"); 
//        joinGame = new JButton("Join Game");
        
        // Hide border when selected
        newGame.setFocusPainted(false);
        endGame.setFocusPainted(false);
        quitGame.setFocusPainted(false);
        saveGame.setFocusPainted(false); 
        loadGame.setFocusPainted(false);
//        hostGame.setFocusPainted(false);
//        joinGame.setFocusPainted(false);
        
		
		menuBar.add(newGame);
		menuBar.add(endGame);
		menuBar.add(saveGame);
		menuBar.add(loadGame);
//		menuBar.add(hostGame);
//        menuBar.add(joinGame);
        menuBar.add(quitGame);
		
		quitGame.addActionListener(gCtrl.new MenuListener(this));
		quitGame.setActionCommand("Quit");
		
		newGame.addActionListener(gCtrl.new MenuListener(this));
		newGame.setActionCommand("New");
		
		endGame.addActionListener(gCtrl.new MenuListener(this));
        endGame.setActionCommand("End");
        endGame.setEnabled(false);
        
        
        //Add the listeners for save game
        saveGame.addActionListener(gCtrl.new MenuListener(this));
		saveGame.setActionCommand("Save");
		saveGame.setEnabled(false);
		
		//Add the listeners for load game
        loadGame.addActionListener(gCtrl.new MenuListener(this));
		loadGame.setActionCommand("Load");
		
//		joinGame.addActionListener(gCtrl.new MenuListener(this));
//		joinGame.setActionCommand("Join");
//        
//        hostGame.addActionListener(gCtrl.new MenuListener(this));
//        hostGame.setActionCommand("Host");

		
	    return menuBar;
	}
	
	public JToolBar createTitlePanel(){
	    
	    this.titleBar = new JToolBar();
	    titleBar.setFloatable(false);
	    titleBar.setPreferredSize(new Dimension(50,40));
	    
	    JLabel scoreLabel = new JLabel("Score: ");
	    JLabel white = new JLabel("White - ");
	    JLabel black = new JLabel(" Black - ");
	    JLabel timeLabel = new JLabel("Time Remaining: ");
	    JLabel pturnlabel = new JLabel("Current Player: ");
	    
	    JLabel[] titleLabels = {scoreLabel, white, black, timeLabel, pturnlabel}; 
	    formatTitleLabel(titleLabels);
	        
	    this.whiteScore = new JLabel("0");
	    this.blackScore = new JLabel("0");
	    this.time = new JLabel("00 : 00: 00 ");
	    this.pturn = new JLabel(" - ");
	    
	    JLabel[] valueLabels = {whiteScore, blackScore, time, pturn};
	    formatValueLabel(valueLabels);

        titleBar.add(new JLabel("                                    "));
	    titleBar.add(scoreLabel);
        titleBar.addSeparator();
        titleBar.add(white);
        titleBar.add(whiteScore);
        titleBar.addSeparator();
        titleBar.add(black);
        titleBar.add(blackScore);
        addSeparator();
        titleBar.add(pturnlabel);
        titleBar.add(pturn);
        titleBar.addSeparator();
        titleBar.addSeparator();
        titleBar.add(timeLabel);
        titleBar.add(time);

	    return titleBar;    
    }
	
	public void formatTitleLabel(JLabel[] labels){
	    for (int i = 0; i < labels.length; i++)
	        labels[i].setFont(new Font("Arial", 0, 14));
    }
	
	public void formatValueLabel(JLabel[] labels){
        for (int i = 0; i < labels.length; i++){
            labels[i].setFont(new Font("Arial", 1, 16));
            labels[i].setForeground(Color.BLUE);
        }
    }
	
	private void addSeparator(){
	    JLabel separate = new JLabel(" || ");
	    JLabel[] separator = {separate};
	    formatTitleLabel(separator);
	    
        titleBar.addSeparator();
        titleBar.addSeparator();
        titleBar.add(separate);
        titleBar.addSeparator();
        titleBar.addSeparator();
    }
  

	public void setWhiteScore(int point){
	    whiteScore.setText(Integer.toString(point));
    }
  
    public void setBlackScore(int point){
        blackScore.setText(Integer.toString(point));
    }
    
    //Method required for timer
    public JLabel getTimeLabel()
    {
        return this.time;

    }
    
    //Method requried to show player turn
    public JLabel getPlayerTurnLabel()
    {
    	return this.pturn;
    }
    
    

    
}
