/* File Name : Main.java
 * 
 * Author(s):
 * Vinayak MANI (S3496268)
 *  
 * File created by Vinu
 */

package Main;

import javax.swing.UIManager;

import Controller.GameController;


public class Main {

	public static void main(String[] args) {
		
		try {
		    UIManager.setLookAndFeel( UIManager.getCrossPlatformLookAndFeelClassName() );
		 } catch (Exception e) {
		            e.printStackTrace();
		 }
		
		// Create new game controller  Comment put in to test Bitbucket
		GameController gameCtrl = new GameController();
		gameCtrl.createGame();
	}

}
